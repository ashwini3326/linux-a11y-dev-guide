The /images directory is preset to be used to store images which will become part of the AsciiDoc documents.
It is configured by :imagesdir:
So you do not need to write whole path to the image.
The image's file name is enough.

Please do not use this directory for any other purposes.
